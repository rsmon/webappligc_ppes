package dto.commande;
import java.util.Date;

public class ResumeCommande {
    
  private Long    numcom;
  private Date    datecom;
  private String  etatcom;
  private Float   montantHT;
  private Float   montantTTC;

  public ResumeCommande(Long numcom, Date datecom, String etatcom, Float montantHT, Float montantTTC){
       
        this.numcom     = numcom;
        this.datecom    = datecom;
        this.etatcom    = etatcom;
        this.montantHT  = montantHT;
        this.montantTTC = montantTTC;
  }

    //<editor-fold defaultstate="collapsed" desc="getters et setters">
    public Long getNumcom() {
        return numcom;
    }
    
    public void setNumcom(Long numcom) {
        this.numcom = numcom;
    }
    
    public Date getDatecom() {
        return datecom;
    }
    
    public void setDatecom(Date datecom) {
        this.datecom = datecom;
    }
    
    public String getEtatcom() {
        return etatcom;
    }
    
    public void setEtatcom(String etatcom) {
        this.etatcom = etatcom;
    }
    
    public Float getMontantHT() {
        return montantHT;
    }
    
    public void setMontantHT(Float montantHT) {
        this.montantHT = montantHT;
    }
    
    public Float getMontantTTC() {
        return montantTTC;
    }
    
    public void setMontantTTC(Float montantTTC) {
        this.montantTTC = montantTTC;
    }
    //</editor-fold>
}



